let express = require('express');
let router = express.Router();
let usuariosControllerAPI = require("../../controllers/api/usuariosControllerAPI");

router.get("/",usuariosControllerAPI.usuario_list);

router.post("/create", usuariosControllerAPI.usuario_create);

router.delete("/delete", usuariosControllerAPI.usuario_delete);

router.put("/:_id/update", usuariosControllerAPI.usuario_update);

router.post("/reservar", usuariosControllerAPI.usuarios_reservar);

module.exports = router;
