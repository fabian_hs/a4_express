let express = require('express');
let router = express.Router();
let reservaControllerAPI = require("../../controllers/api/reservaControllerAPI");

router.get("/",reservaControllerAPI.reserva_list);

router.post("/create", reservaControllerAPI.reserva_create);

router.delete("/delete", reservaControllerAPI.reserva_delete);

router.put("/:_id/update", reservaControllerAPI.reserva_update);

router.post("/reserva", reservaControllerAPI.reserva_diasDeReserva);

module.exports = router;
