let mongoose = require("mongoose");
let Bicicleta = require("../../models/Bicicleta.js");

describe("Testing unitario Bicicletas", function() {

  beforeEach(function(done) {
    var mongoDB = "mongodb://localhost/testdb";
    mongoose.connect(mongoDB, {
      useUnifiedTopology: true,
      useNewUrlParser: true
    });
    var db = mongoose.connection;
    db.on("error", console.error.bind('Error de conxion con MongoDB'));
    db.once("open", function() {
      console.log("Conectado a la BBDD testdb");
      done();
    });
  });

  afterEach(function(done) {
    Bicicleta.deleteMany({}, function(err, succes) {
      if (err) console.log(err);
      done();
    });
  });

  describe("Bicicleta.allBicis", () => {
    it("Empieza vacio", (done) => {
      Bicicleta.allBicis(function(err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });
});

//beforeEach (() => {Bicicleta.allBicis=[]});

/*El beforeEach lo que hace es vaciar el Array de Bicicleta para que las prueba*/

/*
describe("Bicicleta.allBicis", () => {
  it("Empieza sin elementos", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    // La expect espera 0 pero si ya se ha creado dtos el array de Bicicleta dara error, por eso es utíl el beforeEach, por que borrar el array, dejándolo en 0
  });
});

describe("Bicicleta.add", () => {
  it("Añadimos un elemento", () => {
    expect(Bicicleta.allBicis.length).toBe(0);
    let a = new Bicicleta(1,"Rojo", "Trek", [28.503789, -13.853296]);
    Bicicleta.add(a);
    expect(Bicicleta.allBicis.length).toBe(1);
    expect(Bicicleta.allBicis[0]).toBe(a);
  });
});

describe("Bicicleta.findById", () => {
  it("Debe devolver la bici con id 1", () =>{
    expect(Bicicleta.allBicis.length).toBe(0);
    let a = new Bicicleta(1, "Azul", "Urbana", [28.503789, -13.853296]);
    Bicicleta.add(a);
    let targetBici = Bicicleta.findById(1);
    expect(targetBici.id).toBe(1);
    expect(targetBici.color).toBe(a.color);
    expect(targetBici.modelo).toBe(a.modelo);
  });
});

describe("Bicicleta.removeById",() => {
  it("Borrar el array", () =>{

    expect(Bicicleta.allBicis.length).toBe(0); // Espera 0 en el Array de Bicicleta.
    let a = new Bicicleta(1, "Azul", "Urbana", [28.503789, -13.853296]); // Genero un Objeto Bicicleta

    Bicicleta.add(a); // Añade en el array Bicicleta.

    expect(Bicicleta.allBicis.length).toBe(1); // Espera 1 en el Array de Bicicleta.
    let tarfetBici = Bicicleta.removeById(1); // Borra el id 1 del array Bicicleta.
    expect(Bicicleta.allBicis.length).toBe(0); // Espera 0 en el Array de Bicicleta.
  })
});*/
