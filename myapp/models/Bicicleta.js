let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let bicicletaSchema = new Schema({
  bicicletaID: Number,
  color: String,
  modelo: String,
  ubicacion: {
    type: [Number],
    index: true
  }
});

// allBicis
bicicletaSchema.statics.allBicis = function(cb) {
  return this.find({}, cb);
};
// add
bicicletaSchema.statics.add = function(aBici, cb) {
  return this.create(aBici, cb);
};

//findById
bicicletaSchema.statics.findById = function(anId, data, cb) {
  return this.findOne({
    _id: anId
  },data, cb);
};

//RemoveByID
bicicletaSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne({
    _id: anId
  }, cb);
};

bicicletaSchema.statics.findUpdate = function(andId,data,cb){
  return this.findByIdAndUpdate({
    _id: andId
  }, data, cb);
};

module.exports = mongoose.model("bicicleta", bicicletaSchema);
// Antiguo Array
/*let Bicicleta = function(id,color,modelo,ubicacion){
  this.id = id;
  this.color = color;
  this.modelo = modelo;
  this.ubicacion = ubicacion;
}

Bicicleta.allBicis = [];

Bicicleta.add = function (bici) {
  this.allBicis.push(bici);
}

Bicicleta.removeById = function(aBiciId){
  for (let i=0;i< Bicicleta.allBicis.length;i++){

    if(Bicicleta.allBicis[i].id == aBiciId){
      Bicicleta.allBicis.splice(i,1);

      break;
    }
  }
}

Bicicleta.findById = function(aBiciId){
  let aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
  if (aBici){
    return aBici;
  }else{
    throw new Error(`No existe una Bicicleta con el id ${aBiciId}`);
  }
}

let a = new Bicicleta(1,"Rojo","Trek",[28.503789, -13.853296]);
let b = new Bicicleta(2,"Azul","Orbea",[28.501367, -13.853476]);

//Bicicleta.add(a);
//Bicicleta.add(b);

module.exports = Bicicleta;*/
