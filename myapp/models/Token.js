let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let TokenSchema = new Schema ({
  _userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: "Usuario"
  },

  token: {
    type: String,
    required: true
  },

 createAt: {
   type: Date,
   required: true,
   default: Date.now, expires: 43200 //establece el tiempo de vida de el documentos.
 }
});

module.exports = mongoose.model("Token", TokenSchema);
