let mongoose = require("mongoose");
let Schema = mongoose.Schema;

let reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta:{type: mongoose.Schema.Types.ObjectId, ref: "Bicicleta"},
  usuario: {type: mongoose.Schema.Types.ObjectId, ref: "Usuario"}
});

// Cuantos dias esta reservada la bicicleta

reservaSchema.methods.diasDeReserva = function(){
  return moment(this.hasta.diff(moment(this.desde),"days")+1);
};

// allReserva
reservaSchema.statics.allReserva = function(cb) {
  return this.find({}, cb);
};

// add
reservaSchema.statics.add = function(aReserva, cb) {
  return this.create(aReserva, cb);
};

//findById
reservaSchema.statics.findById = function(anId, data, cb) {
  return this.findOne({
    _id: anId
  },data, cb);
};

//RemoveByID
reservaSchema.statics.removeById = function(anId, cb) {
  return this.deleteOne({
    _id: anId
  }, cb);
};

reservaSchema.statics.findUpdate = function(andId,data,cb){
  console.log(andId);
  return this.findByIdAndUpdate({
    _id: andId
  }, data, cb);
};

module.exports = mongoose.model("reserva", reservaSchema);
