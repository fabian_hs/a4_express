var map = L.map('main_map').setView([28.4965, -13.8622],13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',{
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'

}).addTo(map);


L.marker([28.500491, -13.866749]).addTo(map);

L.marker([28.500095, -13.863616]).addTo(map);

L.marker([28.500885, -13.866735]).addTo(map);

$.aja ({
  dataType: "json",

  url: "api/bicicletas",

  success: function(result){
    console.log(result);
    result.bicicletas.forEach(function(bici){
      L.marker(bici.ubicacion,{title: bici.id}).add(map);
    });
  }
});
