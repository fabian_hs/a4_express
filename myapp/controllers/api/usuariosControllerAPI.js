let Usuario = require("../../models/Usuario");

//  usuarios_reservar
exports.usuarios_reservar = function(req, res) {
  Usuario.findById(req.body._id, function(err, usuario) {
    if (err) {
      res.status(500).send(err.message);
    }
    console.log(usuario);
    usuario.reservar(req.body.biciId, req.body.desde, req.body.hasta, function(err) {
      console.log("Reserva!!");
      res.status(200).send();
    });
  });
};

// usuario_list
exports.usuario_list = function(req,res){
  Usuario.allUsuario(function(err,usuario){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(200).send(usuario);
  });
};

// usuario_create
exports.usuario_create = function(req,res){
  Usuario.add({nombre: req.body.nombre},function(err,usuario){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(201).send(usuario);
  });
};

// Usuario_delete
exports.usuario_delete = function(req,res){
  Usuario.removeById(req.body._id,function(err, usuario){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(204).send(usuario);
  });
};

// Update Usuario
exports.usuario_update = function(req,res) {
  Usuario.findUpdate(req.params._id, {
    nombre:req.body.nombre
  }, function(err, usuario){
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(201).send(usuario);
  });
};
