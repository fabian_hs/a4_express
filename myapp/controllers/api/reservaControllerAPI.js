let Reserva = require("../../models/Reserva");

exports.reserva_diasDeReserva = function(req,res){
  Reserva.diasDeReserva(function(err,reser){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(200).send(reser);
  });
};

// usuario_list
exports.reserva_list = function(req,res){
  Reserva.allReserva(function(err,reser){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(200).send(reser);
  });
};

// usuario_create
exports.reserva_create = function(req,res){
  Reserva.add({
    bicicleta: req.body.bicicleta,
    usuario:req.body.usuario,
    desde:req.body.desde,
    hasta: req.body.hasta
     },function(err,reserva){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(201).send(reserva);
  });
};

// Usuario_delete
exports.reserva_delete = function(req,res){
  Reserva.removeById(req.body._id,function(err, reserva){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(204).send(reserva);
  });
};

// Update Usuario
exports.reserva_update = function(req,res) {
  Reserva.findUpdate(req.params._id,{
    desde:req.body.desde,
    hasta: req.body.hasta,
    bicicleta: req.body.bicicleta,
    usuario:req.body.usuario }, function(err, reserva){
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(201).send(reserva);
  });
};
