let Bicicleta = require("../../models/Bicicleta");

exports.bicicleta_list = function(req,res){
  Bicicleta.allBicis(function(err,bicis){
    if(err){
      res.status(500).send(err.message);
    }

    res.status(200).send(bicis);

  });
}

exports.bicicleta_create = function(req,res){
  let bici = ({
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]
  });

  Bicicleta.add(bici,function(err,nbici){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(201).send(nbici);
  });
};

exports.bicicleta_delete = function(req,res){
  Bicicleta.removeById(req.body._id,function(err, bicis){
    if(err){
      res.status(500).send(err.message);
    }
    res.status(204).send(bicis);
  })
}

exports.bicicleta_update = function(req,res) {
  Bicicleta.findUpdate(req.params._id, {
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]
  }, function(err, bicis){
    if (err) {
      res.status(500).send(err.message);
    }
    res.status(201).send(bicis);
  });
}


/*
exports.bicicleta_list = function(req,res){
  res.status(200).json ({
    bicicletas: Bicicleta.allBicis
  });
};

exports.bicicleta_create = function(req,res){

  let bici = new Bicicleta(req.body.id,req.body.color,req.body.modelo);

  bici.ubicacion = [req.body.lantitud, req.body.longitud];

  Bicicleta.add(bici);

  res.status(201).json({
    bicicleta: bici
  });

};

exports.bicicleta_delete = function(req,res){

    Bicicleta.removeById(req.body.id);

    res.status(204).send();

};

exports.bicicleta_update = function(req,res) {
  let bici = Bicicleta.findById(req.params.id);
  bici.id = req.body.id;
  bici.color = req.body.color;
  bici.modelo = req.body.modelo;
  bici.ubicacion = [req.body.lantitud, req.body.longitud];
  res.status(201).json({
    bicicleta:bici
  })
}*/
