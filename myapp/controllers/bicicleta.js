let Bicicleta = require("../models/Bicicleta");

exports.bicicleta_list = function(req, res) {
  Bicicleta.allBicis(function(err, bicis) {
    if (err) {
      res.status(500).send(err.message);
    }
    res.render("bicicletas/index", {
      bicis: bicis
    });
  });
}

exports.bicicleta_create_get = function(req, res) {
  res.render("bicicletas/create");
}

exports.bicicleta_create_post = function(req, res) {
  let bici = ({
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]
  });

  Bicicleta.add(bici, function(err, nbici) {
    if (err) {
      res.status(500).send(err.message);
    }

    res.redirect("/bicicletas");
  });
}



exports.bicicleta_delete_post = function(req, res) {

  Bicicleta.removeById(req.body._id,function(err, bicis) {
      if (err) {
        res.status(500).send(err.message);
      }
      res.redirect("/bicicletas");
    });

}

exports.bicicleta_update_get = function(req, res) {
  let bicis = ({
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]
  });
  Bicicleta.findById(req.params._id, function(err, bicis){
    if (err) {
      res.status(500).send(err.message);
    }
    res.render('bicicletas/update', {
      bici:bicis
    });
  });
}

exports.bicicleta_update_post = function(req, res) {
  //let bici = Bicicleta.findById(req.params._id);

  Bicicleta.findUpdate(req.params._id, {
    bicicletaID: req.body.bicicletaID,
    color: req.body.color,
    modelo: req.body.modelo,
    ubicacion: [req.body.latitud, req.body.longitud]
  }, function(err, bicis){
    if (err) {
      res.status(500).send(err.message);
    }
    res.redirect("/bicicletas");
  });

}
